# OHOS 3.1简明编译流程

梁开祝  2022.04.27

OHOS3.1版本，明显可以看出标准系统的编译流程相比LTS3.0版本，精简了非常多，标准系统的build.sh和小型系统的hb build实际进行了合并统一，我对相关的编译流程做了一下整理，如下图所示。

![OHOS3.1简明编译流程](figures/OHOS3.1简明编译流程.png)

实测通过build.sh编译的log如下：

```
++++++++++++++++++++++++++++++++++++++++
--product-name rk3568 --ccache
[1]//build/build_cripts/build.sh: Begin:
[2]//build/scripts/entry.py: Begin to do_build:
[3]//build/lite/build.py: Begin to build:      { rk3568 }
[4]//build/lite/hb/__main__.py: Begin:
[5]//build/lite/hb_internal/build/build.py: create a 'Build build' and collect cmd+args
[5]//build/lite/hb_internal/build/build.py: build.build( ['rk3568'] )
--------------------------------------------------------------------------
[6]//build/lite/hb_internal/build/build_process.py: build ccache

[OHOS INFO] Set cache size limit to 50.0 GB
--------------------------------------------------------------------------
[6.gn]//build/lite/hb_internal/build/build_process.py: gn_build run gn_gen

[OHOS INFO] [.gn] run .gn -> //build/core/gn/dotfile.gn ==>> root -->>
[OHOS INFO] =======================================================
[OHOS INFO] [gn]  -->>root : //build/core/gn/BUILD.gn: Begin
[OHOS INFO]       root_out_dir      = //out/rk3568
[OHOS INFO]       root_build_dir    = //out/rk3568
[OHOS INFO]       root_gen_dir      = //out/rk3568/gen
[OHOS INFO]       current_toolchain = //build/toolchain/ohos:ohos_clang_arm
[OHOS INFO]       host_toolchain    = //build/toolchain/linux:clang_x64
[OHOS INFO]       enable_ramdisk    = true
[OHOS INFO] [gn]  collect args for 'load.py'
[OHOS INFO] [gn]  _platforms_config_file: //out/preloader/rk3568/platforms.build
[OHOS INFO] [gn]  _subsystem_config_file: //out/preloader/rk3568/subsystem_config.json
[OHOS INFO] [gn]  exec_script: //build/loader/load.py
...........
[OHOS INFO] [gn]  -->>root: //build/core/gn/BUILD.gn: Begin
[OHOS INFO] =======================================================
...........
[OHOS INFO] Done. Made 15929 targets from 2670 files in 71720ms
-------------------------------------------------------------------------------
[6.ninja]//build/lite/hb_internal/build/build_process.py: ninja_build run ninja

[OHOS INFO] [1/1] Regenerating ninja files
[OHOS INFO] [1/728] CC clang_x64/obj/third_party/pcre2/pcre2/src/libpcre2/pcre2_config.o
...........
[OHOS INFO] [639/640] STAMP obj/build/ohos/images/make_images.stamp
[OHOS INFO] [640/640] STAMP obj/build/core/gn/images.stamp

--------------------------------------------------
[6.PostBuild]    build_process.py: PostBuild.clean
[7]//build/lite/hb_internal/common/misc.py: PostBuild.clean()
[OHOS INFO] //build/scripts/summary_ccache_hitrate.py: main()
[OHOS INFO] -------------------------------------------------
[OHOS INFO] ccache summary:
...........
[OHOS INFO] rk3568 build success
[OHOS INFO] cost time: 0:03:05
[6]//build/lite/hb_internal/build/build_process.py: build success
[4]//build/lite/hb/__main__.py: End. status[ 0 ]
[1]//build/build_cripts/build.sh: End.
=====build  successful=====
++++++++++++++++++++++++++++++++++++++++
```

通过hb build来编译，直接就是从[4]开始，到[4]结束。
		中间的过程请小伙伴们自行深入去理解了。