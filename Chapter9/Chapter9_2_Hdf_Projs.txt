这里是OpenHarmony驱动子系统学习过程中用到的示例程序的仓库链接：
1. led_ctrl
    仓库：OhosLedCtrl【这是最早为本书所写的驱动开发示例程序】
    链接：https://gitee.com/liangkzgitee/led_ctrl

2. led_rgb【个人代码仓，与OHOS主干代码仓的led_rgb同步】
    仓库：OhosLedRGB【这是试用DAYU200开发板时整理的示例程序】
    链接：https://gitee.com/liangkzgitee/led_rgb

3. led_rgb【OHOS主干代码仓，与个人代码仓的led_rgb同步】
    仓库：led_rgb【这是提交到主干上的vendor_hihope仓库中的驱动开发示例程序】
    链接：https://gitee.com/openharmony/vendor_hihope/tree/master/rk3568/demo/led_rgb
